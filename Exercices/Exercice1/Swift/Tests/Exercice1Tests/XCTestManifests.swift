import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(Exercice1Tests.allTests),
    ]
}
#endif